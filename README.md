# dat.gui elegant theme

This is a theme for [dat.gui](https://www.npmjs.com/package/dat.gui), that is elegant, slick and nice to look at. \
I got tired of looking at the plain old dat.gui theme so i decided to hack on the themeing to make my eyes not bleed.

## Preview
<div align="center">
	<img src=".screenshots/dat.gui-base.png" width="35%"/>
	&nbsp;
	<img src=".screenshots/dat.gui-themed.png" width="34.3%"/>
</div>

## Usage
Just drop the `dat-gui.css` into your html, and you're set! \
If you want to change any themes, just change the values in `:root`
```css
:root {
    --border-radius: 8px;
    --border-radius-small : 5px;
    --padding: 5px;
    --inner-bar-margin: 0 5px;
    --inner-bar-padding: var(--padding);

    --main-bar-bg : rgb(25, 25, 25);
    --inner-bar-bg : rgb(40, 40, 40);
    --element-bg : rgb(55, 55, 55);
    --slider-fg: rgb(95, 183, 255);
}
```
<i>! For custom left border colors on bars, check the documentation on which classes to pick in the css file</i> \
<i>! To align all folders on the same level uncomment the line:</i>
```css
li.folder {
    border-left: none !important;
}
```

### Todo:
- [ ] Add JS script to change the sliders text input width relative to its content
- [ ] More themes!